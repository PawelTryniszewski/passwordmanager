package pl.sda.szyfrowanie;

abstract class HasloFactory {
    static String generate1() {
        return PasswordGenerator.getPasswordGenerator().generate();
    }

    static String generate2() {
        return PasswordGenerator.getPasswordGenerator().generate2();
    }
}
