package pl.sda.szyfrowanie;

import java.util.Random;

class PasswordGenerator {
    private final static PasswordGenerator passwordGenerator = new PasswordGenerator();

    static PasswordGenerator getPasswordGenerator() {
        return passwordGenerator;
    }
    private PasswordGenerator() {
    }
    String generate() {
        char i;
        char A = 'A';
        char[] tab = new char[60];
        int b = 0;
        for (i = A; i < 125; i++) {
            if (i == '/') {
                tab[b] = --i;
                b++;
            } else {
                tab[b] = i;
                b++;
            }
        }
        Random r = new Random();
        StringBuilder temp = new StringBuilder();
        for (int j = 0; j < 8; j++) {
            temp.append(String.valueOf(r.nextInt(100))).append(String.valueOf(tab[r.nextInt(60)]));
        }
        return temp.toString();
    }

    String generate2() {
        char i;
        char A = '!';
        char[] tab = new char[92];
        int b = 0;
        for (i = A; i < 125; i++) {
            tab[b] = i;
            b++;
        }
        Random r = new Random();
        StringBuilder temp = new StringBuilder();
        for (int j = 0; j < 8; j++) {
            temp.append(String.valueOf(tab[r.nextInt(91)])).append(String.valueOf(tab[r.nextInt(91)]));
        }
        return temp.toString();
    }
}
