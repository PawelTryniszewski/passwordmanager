package pl.sda.szyfrowanie;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws Exception {

        Zapis a = new Zapis();
        Wczytywanie wczytywanie = new Wczytywanie();
        Scanner sc = new Scanner(System.in);
        String komenda="";

        do {
            komenda=sc.nextLine();
            if (komenda.equalsIgnoreCase("add")){
                System.out.println("Podaj nazwe portalu");
                String nazwa = sc.nextLine();
                System.out.println("Wybierz metode szyfrowania: 1 / 2");
                int opcja = sc.nextInt();
                a.toFile(nazwa,opcja);
            }else if (komenda.equalsIgnoreCase("getpassword")){
                System.out.println("do ktorego portalu chcesz uzyskac haslo?");
                System.out.println(wczytywanie.getPassword(sc.nextLine()));
            }else if (komenda.equalsIgnoreCase("schowek")){
                System.out.println("Do jakiego portalu chcesz uzyskac haslo");
                wczytywanie.zapiszHasloDoSchowka(sc.nextLine());
            }else if (komenda.equalsIgnoreCase("wypisz")){
                wczytywanie.wypiszWszystko();
            }
        }while (!komenda.equalsIgnoreCase("quit"));

    }

//    public static String szyfruj(String str) {
//        char x[] = str.toCharArray();
//
//        for (int i = 0; i != x.length; i++) {
//            if (i % 2 == 0) {
//                int n = x[i];
//                n += 2 * x.length / 3;
//                n -= i;
//                x[i] = (char) n;
//            }else {
//                int n = x[i];
//                n += 4 * x.length / 7;
//                n -= i*i;
//                x[i] = (char) n;
//            }
//        }
//        return new String(x);
//    }
//
//    public static String odszyfruj(String str) {
//        char x[] = str.toCharArray();
//
//        for (int i = 0; i != x.length; i++) {
//            if (i % 2 == 0) {
//                int n = x[i];
//                n -= 2 * x.length / 3;
//                n += i;
//                x[i] = (char) n;
//            }
//            else{
//                int n = x[i];
//                n -= 4 * x.length / 7;
//                n += i*i;
//                x[i] = (char) n;
//            }
//        }
//        return new String(x);
//    }
}
